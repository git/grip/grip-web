;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2015
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful, but WITHOUT
;;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;;;; License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(use-modules (globals)
	     (utils))


(define %notes-p
  `,@((h3 "Notes")

     (ol
      (li "In the above configure step, --prefix=/your/prefix is optional. The
default value is /usr/local." (p))

      (li "To install Grip, you must have write permissions to the following
directories:"

	  (ul (@ (class "code"))
	      (li "$prefix and its subdirs")
	      (li "Guile's global site directory")
	      (li "Guile's site ccache directory"))

	  (p "You may check the above two latest directories location using:")

	  (ul (@ (class "code"))
	      (li "guile -c \"(display (%global-site-dir)) (newline)\"")
	      (li "guile -c \"(display (%site-ccache-dir)) (newline)\""))
	
	  (p ,(%grip-a #:name "Grip's") " modules and " (k "Grip-Clutter") "
examples, will be installed in Guile's global site directory. " ,(%grip-a
#:name "Grip's") " compiled modules will be installed in Guile's site ccache
directory."))

      (li "In case you need to augment Guile's " ,(%guile-load-paths-a)
	  " [for your own application(s) for example], you can: (a)
    create/update your personnal ~/.guile file, (b) update Guile's
    global site located init.scm file or (c) define/update your "
    ,(%guile-load-path-var-a) " shell environment variable." (p))

      (li "Like for any other GNU Tool Chain compatible software, you may
    install the documentation locally using make install-info, make
    install-html and/or make install-pdf."))))
