;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2015
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful, but WITHOUT
;;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;;;; License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(use-modules (template)
	     (globals))


;; (include "dependencies-p.scm")
;; (include "notes-p.scm")


(define page
  `((p (@ (class "warn"))
       "... this page still is under construction ...")

    (h2 "Download")

    (p "We're working on " ,(%grip-a #:name "Grip's") " first release, and
therefore there is no " (k "tarball") " yet, but stay tuned!  In the mean
time, see our " (rlink "developers.html" " developers") " page.")

    ))


(define (make-index)
  (output-html page
	       "Grip"
	       "Download"
	       ""
	       #:scm-path "download.scm"))
