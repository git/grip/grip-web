;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2015
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful, but WITHOUT
;;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;;;; License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (utils)
  #:export (%theme-rainbow
	    get-color
	    color-string
	    licstatcontrib))


(define %theme-rainbow
  '("red" "#c18928" #;"yellow" "green" "blue" "magenta"))

(define* (get-color i #:key (theme 'rainbow))
  (case theme
    ((rainbow)
     (list-ref %theme-rainbow
	       (modulo i (length %theme-rainbow))))
    (else
     (error "unknow theme ~a" theme))))

(define* (color-string str #:key (theme 'rainbow))
  (map (lambda (i)
	 `(span (@ (style ,(string-append #;"background-color: black;"
					  "color:"
					  (get-color i #:theme theme))))
		,(string (string-ref str i))))
    (iota (string-length str))))

(define-syntax licstatcontrib
  (syntax-rules ()
    ((licstat ?lic ?stat ?contrib)
     `,@((table (@ (align "right")
		   (style "float:right")) ;; align not supported by html5
		(tr (td (st "License"))
		    (td (st ":"))
		    (td (@ (style "text-align:right"))
			(string-append (inv "-") ,?lic)))
		(tr ;; (@ (bgcolor "#ebe7d0") #;"#e6e2cb")
		    (td (st "Status"))
		    (td (st ":"))
		    (td (@ (style "text-align:right"))
			(string-append (inv "-") (st ,?stat))))
		(tr (td (@ (colspan "3")
			   (style "text-align:right"))
			(fu ,?contrib))))
	  ;; dummy, vert rendering
	 (p (@ (class "invi")) "_")
	 (p (@ (class "invi")) "_")
	 #;(p (@ (class "invi")) "_")))))
