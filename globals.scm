;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2011 - 2016
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Kisê.

;;;; Kisê is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Kisê is distributed in the hope that it will be useful, but WITHOUT
;;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;;;; License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Kisê.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (globals)
  #:export (%alto-a
	    %email-a
	    %kise-a
	    %kise-git-sum-a
	    %kise-git-log-a
	    %kise-latest-a
	    %kise-latest-mdsum-a
	    %kise-latest-sig-a
	    
	    %grip-a
	    %grip-savannah-a
	    %grip-request-a
	    %grip-git-sum-a
	    %grip-git-log-a
	    %grip-developers-a
	    %tactus-a

	    %gnu-a
	    %lgplv2-a
	    %gplv2-a
	    %gplv3-a
	    %savannah-nongnu-a
	    %savannah-a

	    %scheme-a
	    %guile-a
	    %guile-load-paths-a
	    %guile-load-path-var-a
	    %guile-sxml-a
	    %goops-a

	    %gg-a
	    %gg-savannah-a
	    %gg-git-sum-a
	    %gg-git-log-a
	    %gg-git-repo-a
	    %gg-latest-a
	    %gg-latest-sig-a
	    %gg-latest-snapshot-a
	    %gg-other-rel-a

	    %gc-a
	    %gc-git-a
	    %gc-git-log-a
	    %gc-latest-a
	    %gc-latest-sig-a
	    %gc-latest-snapshot-a
	    %gc-other-rel-a

	    %gnome-core-libs-ref-a
	    %guile-lib-a
	    %g-wrap-a
	    %guile-cairo-a
	    %guile-cairo-dev-a
	    %git-a
	    %fsf-a
	    %fsf-philosophy-a
	    %wiki-fp-a
	    %geiser-a
	    %clojure-a
	    %postgresql-a
	    %sqlite-a
	    %winterp-a
	    %pucrio-a
	    %texinfo-a
	    %latex-a
	    %iwona-a
	    %clutter-a
	    %wiki-clutter-a
	    %wiki-scene-graph-a
	    %wiki-retained-mode-a
	    %cogl-a
	    %cpan-a
	    %motif-a

	    %wingolog-a))


;;;
;;; Alto
;;;

(define* (%alto-a #:key (name "Alto Software scrl"))
  `(a (@ (href "http://www.altosw.be"))
      ,name))

(define* (%email-a #:key (name "Alto Software scrl"))
  `(a (@ (href "mailto:david@altosw.be"))
      ,name))


;;;
;;; Savannah
;;;

(define* (%savannah-a #:key (name "GNU Savannah"))
  `(a (@ (href "http://savannah.gnu.org/"))
      ,name))

(define* (%grip-a #:key (name "Grip"))
  `(a (@ (href "index.html"))
      ,name))

(define* (%grip-savannah-a #:key (name "Grip"))
  `(a (@ (href "http://savannah.nongnu.org/projects/grip"))
      ,name))

(define* (%grip-request-a #:key (name "Grip"))
  `(a (@ (href "http://savannah.gnu.org/task/?13771"))
      ,name))

(define* (%grip-git-sum-a #:key (name "Grip"))
  `(a (@ (href "http://git.savannah.gnu.org/cgit/grip.git"))
      ,name))

(define* (%grip-git-log-a #:key (name "Grip"))
  `(a (@ (href "http://git.savannah.gnu.org/cgit/grip.git/log/"))
      ,name))

(define* (%grip-developers-a #:key (name "the Grip developers"))
  `(a (@ (href "mailto:<david at altosw dot be>"))
      ,name))


;;;
;;; Kisê
;;;

(define* (%kise-a #:key (name "Kisê"))
  `(a (@ (href "http://www.altosw.be/kise/index.html"))
      ,name))

(define* (%kise-git-sum-a #:key (name "Kisê's git summary"))
  `(a (@ (href ""))
      ,name))

(define* (%grip-git-log-a #:key (name "Kisê's git log"))
  `(a (@ (href ""))
      ,name))

(define* (%kise-latest-a #:key (name "kise-0.9.4.tar.gz"))
  `(a (@ (href "http://altosw.be/kise/releases/kise-0.9.4.tar.gz"))
      ,name))

(define* (%kise-latest-mdsum-a #:key (name "MD5 checksums"))
  `(a (@ (href "http://altosw.be/kise/releases/kise-0.9.4.md5sums"))
      ,name))

(define* (%kise-latest-sig-a #:key (name "kise-0.9.4.tar.gz.sig"))
  `(a (@ (href "http://altosw.be/kise/releases/kise-0.9.4.tar.gz.sig"))
      ,name))

(define* (%kise-other-releases-a #:key (name "here"))
  `(a (@ (href "http://altosw.be/kise/releases/"))
      ,name))


;;;
;;; Tactus
;;;

(define* (%tactus-a #:key (name "Tactus"))
  `(a (@ (href "tactus/tactus.html"))
      ,name))


(define* (%gnu-a #:key (name "GNU"))
  `(a (@ (href "http://www.gnu.org/"))
      ,name))

(define* (%lgplv2-a #:key (name "LGPLv2"))
  `(a (@ (href "http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html"))
      ,name))

(define* (%gplv2-a #:key (name "GPLv2"))
  `(a (@ (href "http://www.gnu.org/licenses/old-licenses/gpl-2.0.html"))
      ,name))

(define* (%gplv3-a #:key (name "GPLv3"))
  `(a (@ (href "http://www.gnu.org/licenses/quick-guide-gplv3.html"))
      ,name))

(define* (%savannah-nongnu-a #:key (name "Savannah non gnu"))
  `(a (@ (href "http://savannah.nongnu.org/"))
      ,name))

(define* (%scheme-a #:key (name "Scheme"))
  `(a (@ (href "http://www.scheme.org/"))
      ,name))

(define* (%guile-a #:key (name "Guile"))
  `(a (@ (href "http://www.gnu.org/software/guile/"))
      ,name))

(define* (%guile-load-paths-a #:key (name "load paths"))
  `(a (@ (href  "http://www.gnu.org/software/guile/manual/guile.html#Load-Paths"))
      ,name))

(define* (%guile-load-path-var-a #:key (name "GUILE_LOAD_PATH"))
  `(a (@ (href "https://www.gnu.org/software/guile/manual/guile.html#index-GUILE_005fLOAD_005fPATH"))
      ,name))

(define* (%guile-sxml-a #:key (name "sxml"))
  `(a (@ (href "http://www.gnu.org/software/guile/manual/guile.html#SXML"))
      ,name))

(define* (%goops-a #:key (name "Goops"))
  `(a (@ (href "http://www.gnu.org/software/guile/manual/guile.html#GOOPS"))
      ,name))


(define* (%gg-a #:key (name "Guile-Gnome"))
  `(a (@ (href "http://www.gnu.org/software/guile-gnome/"))
      ,name))

(define* (%gg-savannah-a #:key (name "GNU Savannah project page"))
  `(a (@ (href "http://savannah.gnu.org/projects/guile-gnome"))
      ,name))

(define* (%gg-git-sum-a #:key (name "git summary"))
  `(a (@ (href "http://git.savannah.gnu.org/cgit/guile-gnome.git/"))
      ,name))

(define* (%gg-git-log-a #:key (name "git log"))
  `(a (@ (href "http://git.savannah.gnu.org/cgit/guile-gnome.git/log/"))
      ,name))

(define* (%gg-git-repo-a #:key (name "Savannah"))
  `(a (@ (href "http://git.savannah.gnu.org/cgit/guile-gnome.git"))
      ,name))

(define* (%gg-latest-a #:key (name "guile-gnome-platform-2.16.3.tar.gz"))
  `(a (@ (href "http://ftp.gnu.org/pub/gnu/guile-gnome/guile-gnome-platform/guile-gnome-platform-2.16.3.tar.gz"))
      ,name))

(define* (%gg-latest-sig-a #:key (name "guile-gnome-platform-2.16.3.tar.gz.sig"))
  `(a (@ (href "http://ftp.gnu.org/pub/gnu/guile-gnome/guile-gnome-platform/guile-gnome-platform-2.16.3.tar.gz.sig"))
      ,name))

(define* (%gg-latest-snapshot-a #:key (name "guile-gnome-2.16.3.tar.gz"))
  `(a (@ (href "http://git.savannah.gnu.org/cgit/guile-gnome.git/snapshot/guile-gnome-2.16.3.tar.gz"))
      ,name))

(define* (%gg-other-rel-a #:key (name "Guile-Gnome releases"))
  `(a (@ (href "http://ftp.gnu.org/pub/gnu/guile-gnome/guile-gnome-platform/"))
      ,name))


;;;
;;; Guile Cluter
;;;

(define* (%gc-a #:key (name "Guile-Clutter"))
  `(a (@ (href "http://www.gnu.org/software/guile-gnome/clutter"))
      ,name))
(define* (%gc-git-a #:key (name "git summary"))
  `(a (@ (href "http://git.savannah.gnu.org/cgit/guile-gnome.git/?h=clutter"))
      ,name))

(define* (%gc-git-log-a #:key (name "git log"))
  `(a (@ (href "http://git.savannah.gnu.org/cgit/guile-gnome.git/log/?h=clutter"))
      ,name))

(define* (%gc-latest-a #:key (name "guile-clutter-1.12.2.tar.gz"))
  `(a (@ (href "http://ftp.gnu.org/pub/gnu/guile-gnome/guile-gnome-platform/guile-clutter-1.12.2.tar.gz"))
      ,name))

(define* (%gc-latest-sig-a #:key (name "guile-clutter-1.12.2.tar.gz.sig"))
  `(a (@ (href "http://ftp.gnu.org/pub/gnu/guile-gnome/guile-gnome-platform/guile-clutter-1.12.2.tar.gz.sig"))
      ,name))

(define* (%gc-latest-snapshot-a #:key (name "guile-clutter-1.12.2.tar.gz"))
  `(a (@ (href "http://git.savannah.gnu.org/cgit/guile-gnome.git/snapshot/guile-clutter-1.12.2.tar.gz"))
      ,name))

(define* (%gc-other-rel-a #:key (name "Guile-Clutter releases"))
  `(a (@ (href "http://ftp.gnu.org/pub/gnu/guile-gnome/guile-gnome-platform/"))
      ,name))


(define* (%gnome-core-libs-ref-a #:key (name "GNOME core libraries"))
  `(a (@ (href "http://developer.gnome.org/references"))
      ,name))

(define* (%guile-lib-a #:key (name "Guile-Lib"))
  `(a (@ (href "http://www.nongnu.org/guile-lib/"))
      ,name))

(define* (%g-wrap-a #:key (name "G-Wrap"))
  `(a (@ (href "http://www.nongnu.org/g-wrap/"))
      ,name))

(define* (%guile-cairo-a #:key (name "Guile-Cairo"))
  `(a (@ (href "http://www.nongnu.org/guile-cairo/"))
      ,name))

(define* (%guile-cairo-dev-a #:key (name "Guile-Cairo"))
  `(a (@ (href "http://www.nongnu.org/guile-cairo/dev/"))
      ,name))

(define* (%git-a #:key (name "Git"))
  `(a (@ (href "http://git-scm.org/"))
      ,name))

(define* (%fsf-a #:key (name "Free Software Foundation"))
  `(a (@ (href "http://www.fsf.org/"))
      ,name))

(define* (%fsf-philosophy-a #:key (name "Philosophy of the GNU Project"))
  `(a (@ (href "http://www.gnu.org/philosophy/philosophy.html"))
      ,name))

(define* (%wiki-fp-a #:key (name "functional programming"))
  `(a (@ (href "http://en.wikipedia.org/wiki/Functional_programming"))
      ,name))

(define* (%geiser-a #:key (name "Geiser"))
  `(a (@ (href "http://www.nongnu.org/geiser/"))
      ,name))

(define* (%clojure-a #:key (name "Clojure"))
  `(a (@ (href "http://clojure.org/"))
      ,name))

(define* (%clojure-a #:key (name "Clojure"))
  `(a (@ (href "http://clojure.org/"))
      ,name))

(define* (%postgresql-a #:key (name "PostgreSQL"))
  `(a (@ (href "http://www.postgresql.org/"))
      ,name))

(define* (%sqlite-a #:key (name "SQLite"))
  `(a (@ (href "http://sqlite.org/"))
      ,name))

(define* (%winterp-a #:key (name "Winterp"))
  `(a (@ (href "http://nielsmayer.com/winterp/"))
      ,name))

(define* (%pucrio-a #:key (name "PUC-Rio"))
  `(a (@ (href "http://www.puc-rio.br/english/"))
      ,name))

(define* (%texinfo-a #:key (name "Texinfo"))
  `(a (@ (href "http://www.gnu.org/software/texinfo/"))
      ,name))

(define* (%latex-a #:key (name "LaTex"))
  `(a (@ (href "http://www.latex-project.org/"))
      ,name))

(define* (%iwona-a #:key (name "Iwona"))
  `(a (@ (href "http://www.tug.dk/FontCatalogue/iwona/"))
      ,name))

(define* (%clutter-a #:key (name "Clutter"))
  `(a (@ (href "http://blogs.gnome.org/clutter/"))
      ,name))

(define* (%wiki-clutter-a #:key (name "GNOME Clutter"))
  `(a (@ (href "https://en.wikipedia.org/wiki/Clutter_(software)"))
      ,name))

(define* (%wiki-scene-graph-a #:key (name "Scene Graph"))
  `(a (@ (href "https://en.wikipedia.org/wiki/Scene_graph"))
      ,name))

(define* (%wiki-retained-mode-a #:key (name "Retained Mode"))
  `(a (@ (href "https://en.wikipedia.org/wiki/Retained_mode"))
      ,name))

(define* (%cogl-a #:key (name "Cogl"))
  `(a (@ (href "http://cogl3d.org/index.html"))
      ,name))

(define* (%cpan-a #:key (name "CPAN"))
  `(a (@ (href "http://www.cpan.org/"))
      ,name))

(define* (%motif-a #:key (name "Motif"))
  `(a (@ (href "http://www.opengroup.org/desktop/motif.html"))
      ,name))

(define* (%wingolog-a #:key (name "Andy Wingo"))
  `(a (@ (href "http://www.wingolog.org"))
      ,name))
