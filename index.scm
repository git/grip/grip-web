;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2015
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful, but WITHOUT
;;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;;;; License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(use-modules (ice-9 match)
	     (template)
	     (globals)
	     (utils))


(define page
  `((h2 "About")

    (p ,(%grip-a) ", a " (k "Grip of Really Important Procedures") ", is a "
,(%guile-a) " scheme toolbox currently composed of " (k "Grip") "
itself, " (k "Grip-SQLite") ", " (k "Grip-Gnome") " and " (k "Grip-Clutter")
".")

    (p ,(%grip-a) ", " (k "Grip-SQLite") " and " (k "Grip-Gnome") " are
mostly used by " ,(%kise-a) ", " (k "a small and easy to use time keeping
application") " entirely written in " ,(%guile-a) ". ")

    (p (@ (class "corenote")) (b "Note: ")
,(%grip-a) ", " (k "Grip-SQLite") " and " (k "Grip-Gnome") " need to be and
will be rewritten, I started already, you'll spot that by yourself in the
code if you visit it: indeed, they come from old times, when I could not
trust the Guile's module system, " ,(%goops-a) " ... and myself [as a
schemer :)].")

    (p (k "Grip-Clutter") " is recent and reasonably well designed and
coded, despite some " ,(%goops-a) " problems, which we're working on with "
,(%guile-a #:name "Guile's") " maintainers, but it still needs more and some
app(s) to proove this. So no fixed "
 (k "API") " yet either. It has some good " ,(%clutter-a) " examples, more to
come soon.")

    (p "So, all this said, you are welcome to try and use " ,(%grip-a) ",
keeping the above in mind, come and help " #;,(%alto-a #:name "us") " to get
it better designed, coded and debugged, thanks!")

    (h2 "Latest News")

    (latest-news)

    (p "See Grip's " ,(%grip-git-sum-a #:name "git summary") " and "
,(%grip-git-log-a #:name "git log") " for further details.")

    (p "This and older news are available"
       (rlink "news.html" " here") ".")

    (h2 "Savannah")

    (p ,(%grip-a) " also has a [non GNU] " ,(%grip-savannah-a
#:name "Savannah project page."))

    (h2 "License")

    (p ,(%grip-a) " is a free software, distributed under the terms of the
GNU General Public License " ,(%gplv3-a) " or higher. You must be aware
there is no warranty whatsoever for " ,(%grip-a) ". This is described in full in the
licenses.")))

(define this-page page)
(load "news.scm")
(define news-page page)
(define page this-page)

(define (latest-news . body)
  (match news-page
    ((title latest . rest) latest)))

(define (make-index)
  (output-html page
	       "Grip"
	       "Grip"
	       ""
               #:transform-rules `((news . ,news)
                                   (latest-news *macro* . ,latest-news))))
