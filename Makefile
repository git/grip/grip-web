
####
#### Copyright (C) 2015
#### David Pirotte <david at altosw dot be>

#### This file is part of Grip.

#### Grip is free software: you can redistribute it and/or modify it
#### under the terms of the GNU General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or
#### (at your option) any later version.

#### Grip is distributed in the hope that it will be useful, but
#### WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#### General Public License for more details.

#### You should have received a copy of the GNU General Public License
#### along with Grip.  If not, see <http://www.gnu.org/licenses/>.
####

### Commentary:

### Code:


SCM_SRC := $(shell find . -name index.scm | grep -v '{arch}') \
	download.scm		\
	developers.scm		\
	news.scm		\
	examples.scm		\
	documentation.scm	\
	contact.scm

HTML = $(subst .scm,.html,$(SCM_SRC))

# nasty hack!
top_srcdir = /usr/local/src/grip/git-web

all: $(HTML)

index.html: index.scm news.scm

$(HTML): %.html: %.scm
	( cd `dirname $<`; $(top_srcdir)/env guile --debug -l `basename $<` -c '(setlocale LC_ALL "") (make-index)' > `basename $@` || { rm `basename $@`; false; } )

clean:
	rm -f $(HTML)
