;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2015
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful, but WITHOUT
;;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;;;; License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(define-module (template)
  #:use-module (ice-9 optargs)
  #:use-module (sxml simple)
  #:use-module (sxml transform)
  #:use-module (globals)
  #:use-module (utils)

 
  #:export (news
	    output-html))


(define* (templatize body title category relative-root-path
		     #:key (scm-path "index.scm"))
  
  (define (href . args)
    `(href ,(apply string-append relative-root-path args)))

  (define (list-join l infix)
    "Infixes @var{infix} into list @var{l}."
    (if (null? l) l
        (let lp ((in (cdr l))
		 (out (list (car l))))
          (cond ((null? in) (reverse out))
                (else
		 (lp (cdr in) (cons* (car in) infix out)))))))

  (define (make-navbar)
    `(div (@ (id "menu"))
          ,@(list-join
             (map (lambda (x)
		    (if (string-ci=? category (car x))
			`(m ,(car x))
			`(a (@ ,(href (cdr x))) ,(car x))))
	       '(("Download" . "download.html")
		 ("Developers" . "developers.html")
		 ("News" . "news.html")
		 ("Examples" . "examples.html")
		 ("Documentation" . "documentation.html")
		 ("Contact" . "contact.html")))
             ""))) ;; we don't use it, quick hack for now

  `(html
    (head (title ,title)
          (meta (@ (name "Generator")
                   (content "The Guile SXML Toolkit")))
	  (meta (@ (name "Content-Type")
                   (content "text/html")
		   (charset "utf-8")))
          (link (@ (rel "stylesheet") (type "text/css")
                   ,(href "base.css"))))
    (body
     (div (@ (id "wrap"))
          (div (@ (id "header"))
	       (div (@ (id "title"))
		    (p (@ (class "title"))
		       (a (@ ,(href "index.html"))
			  #;(img (@ (class "logo")
			  (src "images/alto-h140.png")
			  (align "bottom")))
			  "Grip"))
		    (p (@ (class "subtitle"))
		       "a Grip of Really Important Procedures"))
	       ,(make-navbar))
	  (div (@ (id "inner"))
               ,@body))
     (div (@ (id "footer"))
	  "© 2015 " ,(%grip-developers-a)
	  (br)
	  "Powered by " ,(%guile-sxml-a)))))

(define *xhtml-doctype*
  (string-append
   "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" "
   "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"))

(define (preprocess page transform-rules relative-root-path)
  (define (rlink tag path . body)
    `(a (@ (href ,(string-append relative-root-path path))) ,body))
  (pre-post-order
   page
   `(,@transform-rules
     (rlink . ,rlink)
     (*text* . ,(lambda (tag text) text))
     (*default* . ,(lambda args args)))))

(define (news tag args . body)
  `(div (h4 ,@(assq-ref (cdr args) 'date))
        (p ,@body)))

(define* (output-html page title category relative-root-path
                      #:key (scm-path "index.scm")
		      (transform-rules '()))
  (display *xhtml-doctype*)
  (sxml->xml
   (templatize (preprocess page transform-rules relative-root-path)
               title
	       category
	       relative-root-path
	       #:scm-path scm-path)))
