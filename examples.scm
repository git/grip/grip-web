;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2015
;;;; David Pirotte <david@altosw.be> 

;;;; This file is part of Alto Software's sxml web-pages.

;;;; Alto Software's sxml web-pages is free software: you can
;;;; redistribute it and/or modify it under the terms of the GNU General
;;;; Public License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; Alto Software's sxml web-pages is distributed in the hope that it
;;;; will be useful, but WITHOUT ANY WARRANTY; without even the implied
;;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;;; See the GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Guile-Gnome.  If not, see
;;;; <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(use-modules (template)
	     (globals)
	     (utils))


#;(include "projects/kise-p.scm")


(define page
  `(;;(div (@ (id "twocol"))
	 (h1 "Examples")
	 
	 (p "Some " (k "Grip-Clutter") " examples, just screen copies really,
that we are working on as we design and develop a " (k "Grip-Clutter") "
toolbox. "(k "Grip-Clutter") " examples are installed
in: " (code "(string-append (%global-site-dir) \"/grip/clutter/examples\"") ").")

	 (p "So far the following examples have been developed: "
	    (k "bouncer") ", " (k "clock") ", " (k "grid") ", " (k "spline") ", " (k "toolbar") ", " (k "scroll") ", " (k "drag") ", " (k "drop") " and a " (k "template")
	    " users can take advantage of to make a super quick " ,(%gc-a) ", " (k "Grip-Clutter") " start. All examples respond to the the following same options: ")

	 (ul (@ (class "code"))
	     (li "--debug"
		 (ul (@ (class "code"))
		     "spawns a server that you may connect to in order to interact with the
clutter example that is being run"))

	     (li "--help"
		 (ul (@ (class "code"))
		     "display this usage information"))

	     (li --version
		 (ul (@ (class "code"))
		     "display version information"))
	     )

	 (p "Here is a complete session example calling " (k "./bouncer --debug"))

	 (ul (@ (class "code"))
	     (li "./bouncer --debug")
	     (ul (@ (class "code"))
		 (li "Grip-Clutter 1.12.2.1:  The bouncer example.")
		 (li "Copyright (C) 2015 ...")
		 (br)
		 (li "You requested to run this grip-clutter example in debug mode, here
is what I did for you:")
		 (br)
		 (li "[-] a server has been spawned, in a new thread, port 1968, which
you may connect to [if you use emacs and geiser, which we recommend, use M-x
connect-to-guile];")
		 (br)
		 (li "[-] the following global variables have been defined:")
		 (br)
		 (ul (@ (class "code"))
		     (li "*stage*      a <clus-stage> instance")
		     (li "*clue*       a <clue> instance")
		     (li "*bouncer*    a <clus-bouncer> instance"))))

	 (p "Following, just [50% reduced] screen copies of the above listed examples.")

	 (h3 "The Bouncer")
	 #;(img (@ (class "center")
		 (src "images/terminal-50p.png")))	 
	 (img (@ (class "center")
		 (src "images/bouncer-50p.png")))
	 
	 (h3 "The Clock")
	 (img (@ (class "center")
		 (src "images/clock-50p.png")))
	 
	 (h3 "The Grid")
	 (img (@ (class "center")
		 (src "images/grid-50p.png")))

	 (h3 "The Spline")
	 (img (@ (class "center")
		 (src "images/spline-50p.png")))

	 (h3 "The Toolbar")
	 (img (@ (class "center")
		 (src "images/toolbar-west-50p.png")))

	 (h3 "The Scroll")
	 (img (@ (class "center")
		 (src "images/scroll-50p.png")))

	 (h3 "Drag")
	 (img (@ (class "center")
		 (src "images/drag-50p.png")))

	 (h3 "Drop")
	 (img (@ (class "center")
		 (src "images/drop-50p.png")))

	 ))


(define (make-index)
  (output-html page
	       "Grip"
	       "Examples"
	       ""
	       #:scm-path "examples.scm"))
