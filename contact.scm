;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2015
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful, but WITHOUT
;;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;;;; License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(use-modules (template)
	     (globals))


(define page
  `((h2 "Contact")

    (p ,(%grip-a) " does not have a " (k "mailing list") " yet.  Please send
your " (k "questions") ", " (k "suggestions") ", " (k "requests for
improvement") ", and " (k "ideas") " to one or both the following
addresses:")

    (p (@ (class "enote"))
       "david at altosw dot be"(br)
       "guile-user at gnu dot org")

    
    (h2 "Bugs")

    (p ,(%grip-a) " does not have a " (k "bug tracker") " yet.  Please send
your " (k "bug reports") " to one or both the following addresses:")

    (p (@ (class "enote"))
       "david at altosw dot be"(br)
       "guile-user at gnu dot org")
    
    (h2 "IRC")

    (p "Most of the time you can find me on irc, channel " (k "#guile") ", "
(k "#guix") " and " (k "#scheme") " on irc.freenode.net, " (k "#clutter") " on
irc.gnome.org, under the nickname " (code "daviid") ".")))


(define (make-index)
  (output-html page
	       "Grip"
	       "Contact" ""
	       #:scm-path "contact.scm"))
