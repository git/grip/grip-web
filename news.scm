;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2015
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful, but WITHOUT
;;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;;;; License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(use-modules (template)
	     (globals))

(define page
  `((h2 "Latest News")

    (news
     (@ (date "15 November 2015")
        (title "Working on our first release"))

     (p "We are currently working on " ,(%grip-a #:name "Grip's") " first
release, stay tuned!  In the mean time, see our " (rlink "developers.html" "
developers") " page."))
    
    (h2 "Older News")

    (news (@ (date "12 Octobre 2015")
	     (title "We're on Savannah [nongnu]!"))

	  (p "Our request for a nongnu project page at Savannah for our "
,(%grip-a) " toolbox is accepted, with all the honors! Bruno Félix Rezende
Ribeiro, the GNU Savannah hacker in charge of our " ,(%grip-request-a
#:name "evaluation") " said:")

	  (p (@ (class "corenote"))

	     "... I wish every package submitted to Savannah were so easy to evaluate
as your. Congratulations for the high level of compliance! ..."))))


(define (make-index)
  (output-html page
	       "Grip"
	       "News"
	       ""
	       #:scm-path "news.scm"
               #:transform-rules `((news . ,news))))
