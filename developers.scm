;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2015
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful, but WITHOUT
;;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;;;; License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(use-modules (template)
	     (globals))


(include "dependencies-p.scm")
(include "notes-p.scm")


(define page
  `((h2 "Dependencies")

    ,%dependencies-p
	
    (h2 "Quickstart")

     (p ,(%grip-a) " uses " ,(%git-a) " for revision control. The most
recent sources can be found at " ,(%grip-git-sum-a #:name "here") ".")

     (p "There are currently 2 [important] branches: " (code "master") "
and " (code "devel") ". " ,(%grip-a #:name "Grip's") " stable branch is "
 (code "master") ", developments occur on the " (code "devel") "
branch.")

     (p "So, to grab, compile and install from the source, open a terminal
and:")

     (ul (@ (class "code"))
	 (li "git clone git://git.savannah.nongnu.org/grip.git")
	 (li "cd grip")
	 (li "./autogen.sh")
	 (li "./configure [--prefix=/your/prefix]")
	 (li "make")
	 (li "make install"))
     
     (p "The above steps ensure you're using " ,(%grip-a #:name "Grip's") "
bleeding edge stable version. If you wish to participate to developments,
checkout the " (k "devel") " branch:")

     (ul (@ (class "code"))
	 (li "git checkout --track -b devel origin/devel"))
	 
     (p "Happy hacking!")

     ,%notes-p))


(define (make-index)
  (output-html page
	       "Grip"
	       "Developers"
	       ""
	       #:scm-path "developers.scm"))
