;; -*- mode: scheme; coding: utf-8 -*-

;;;;
;;;; Copyright (C) 2015
;;;; David Pirotte <david at altosw dot be>

;;;; This file is part of Grip.

;;;; Grip is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; Grip is distributed in the hope that it will be useful, but WITHOUT
;;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;;;; License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with Grip.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;;; Commentary:

;;; Code:


(use-modules (globals)
	     (utils))


(define %dependencies-p

  `,@((p ,(%grip-a) " will look for the following dependencies and will
try to install all its components, but will only do so if it can
effectively install " ,(%grip-a) " core and statisfy the component specific
requirement(s).")

      (p (@ (class "one"))
	 (k "Grip:"))

      (ul (@ (class "code"))
	  (li "Autoconf >= 2.69")
	  (li "Automake >= 1.14")
	  (li ,(%guile-a #:name "Guile-2.0") " >= 2.0.11 or Guile-2.2"))

      (p (@ (class "one"))
	 (k "Grip-SQLite:"))

      (ul (@ (class "code"))
	  (li ,(%sqlite-a) " >= 3.7"))

      (p (@ (class "one"))
	 (k "Grip-Gnome:"))

      (ul (@ (class "code"))
	  (li ,(%gg-a) " >= 2.16.4, the following wrappers:")
	  (ul (@ (class "code"))
	      (p "Gobject, Glib, Pango, Pangocairo, libgnome, lingnomeui, libglade,
Gtk")))

      (p (@ (class "one"))
	 (k "Grip-Clutter:"))

      (ul (@ (class "code"))
	  (li ,(%gc-a) " >= 1.12.2.1"))

      ))
